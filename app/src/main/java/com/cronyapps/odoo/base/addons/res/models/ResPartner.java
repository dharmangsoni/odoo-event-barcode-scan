package com.cronyapps.odoo.base.addons.res.models;

import android.content.Context;

import com.cronyapps.odoo.R;
import com.cronyapps.odoo.api.wrapper.helper.OdooUser;
import com.cronyapps.odoo.core.orm.BaseDataModel;
import com.cronyapps.odoo.core.orm.annotation.DataModel;
import com.cronyapps.odoo.core.orm.type.FieldBlob;
import com.cronyapps.odoo.core.orm.type.FieldChar;

@DataModel("res.partner")
public class ResPartner extends BaseDataModel<ResPartner> {

    public FieldChar name = new FieldChar("Name").required();
    public FieldBlob image_medium = new FieldBlob("Image").defaultValue("false");

    public ResPartner(Context context, OdooUser user) {
        super(context, user);
    }

    @Override
    public String authority() {
        return getContext().getString(R.string.app_customer_authority);
    }

}
