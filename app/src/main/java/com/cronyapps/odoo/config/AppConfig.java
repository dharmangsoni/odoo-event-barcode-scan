package com.cronyapps.odoo.config;

public class AppConfig {

    public static Boolean ALLOW_MULTI_ACCOUNT = true;

    public static String[] USER_GROUPS = {"base.group_user"};
}
