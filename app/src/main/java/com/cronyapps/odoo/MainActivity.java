package com.cronyapps.odoo;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cronyapps.odoo.addons.event.BarcodeScanActivity;
import com.cronyapps.odoo.addons.event.models.EventEvent;
import com.cronyapps.odoo.addons.event.models.EventRegistration;
import com.cronyapps.odoo.api.OdooApiClient;
import com.cronyapps.odoo.api.wrapper.handler.gson.OdooResult;
import com.cronyapps.odoo.api.wrapper.helper.OArguments;
import com.cronyapps.odoo.api.wrapper.helper.OdooUser;
import com.cronyapps.odoo.api.wrapper.impl.IOdooResponse;
import com.cronyapps.odoo.base.addons.res.models.ResPartner;
import com.cronyapps.odoo.core.helper.CronyActivity;
import com.cronyapps.odoo.core.orm.sync.utils.AppSyncUtils;
import com.cronyapps.odoo.core.utils.BitmapUtils;

import org.json.JSONArray;
import org.json.JSONObject;

public class MainActivity extends CronyActivity implements View.OnClickListener {
    private EventRegistration registration;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_main_activity);
        registration = new EventRegistration(this, null);
        findViewById(R.id.btnScanCode).setOnClickListener(this);

    }

    private void calculateScanned() {
        int scanned = registration.count("state = ?", "done");
        int total_attendee = registration.count(null, null);
        String msg = getString(R.string.scanned_status, scanned, total_attendee);
        TextView status = (TextView) findViewById(R.id.scanStatus);
        status.setText(msg);
    }


    @Override
    protected void onResume() {
        super.onResume();
        AppSyncUtils.get(registration).requestSync(registration.authority());
        calculateScanned();
    }

    @Override
    public void onClick(View view) {
        startActivityForResult(new Intent(this, BarcodeScanActivity.class), 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == 1) {
            String barcode = data.getExtras().getString(BarcodeScanActivity.KEY_CONTENT);
            EventRegistration record = registration.select(null, "barcode = ?", new String[]{barcode}, null);
            if (record.size() <= 0) {
                Toast.makeText(this, R.string.no_attendee_found, Toast.LENGTH_SHORT).show();
            } else {
                showAttendee(record.getAt(0));
            }
        }
    }

    private void showAttendee(final EventRegistration values) {
        findViewById(R.id.attendeePreview).setVisibility(View.VISIBLE);
        TextView name = (TextView) findViewById(R.id.attendeeName);
        name.setText(values.name.getValue());
        TextView event = (TextView) findViewById(R.id.eventName);
        event.setText(values.event_id.<EventEvent>read().name.getValue());
        ImageView image = (ImageView) findViewById(R.id.attendeeImage);
        ResPartner partner = values.partner_id.read();
        if (partner != null && !partner.image_medium.getValue().equals("false")) {
            image.setImageBitmap(BitmapUtils.getBitmapImage(partner.image_medium.getValue()));
        } else {
            image.setImageResource(R.drawable.user_default);
        }
        TextView title = (TextView) findViewById(R.id.registerAttendee);
        findViewById(R.id.registerAttendee).setOnClickListener(null);
        if (values.state.getValue().equals("done")) {
            title.setText(getString(R.string.already_attented));
            title.setBackgroundColor(Color.parseColor("#8AC249"));

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    findViewById(R.id.attendeePreview).setVisibility(View.GONE);
                }
            }, 5000);

        } else {
            title.setText(getString(R.string.label_register_attendee));
            title.setBackgroundResource(R.color.colorAccent);
            findViewById(R.id.registerAttendee).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new RegisterAttendee().execute(values);
                }
            });
        }
    }

    private class RegisterAttendee extends AsyncTask<EventRegistration, Void, Void> {

        @Override
        protected Void doInBackground(final EventRegistration... regs) {
            try {
                OdooApiClient client = new OdooApiClient.Builder(MainActivity.this)
                        .setUser(OdooUser.get(MainActivity.this))
                        .synchronizedRequests().build();
                OArguments arguments = new OArguments();
                arguments.add(new JSONArray().put(regs[0].id.getValue()));
                JSONObject context = new JSONObject();
                context.put("active_id", regs[0].event_id.getValue());
                context.put("active_ids", new JSONArray().put(regs[0].event_id.getValue()));
                arguments.add(context);
                client.callMethod("event.registration", "button_reg_close", arguments, null, null, new IOdooResponse() {
                    @Override
                    public void onResult(OdooResult result) {
                        regs[0].state.setValue("done");
                        regs[0].update();
                        Log.e(">>", regs + "<<");
                    }

                    @Override
                    public boolean onError(OdooResult error) {
                        Log.e(">>", error + "<<");
                        return super.onError(error);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            calculateScanned();
            findViewById(R.id.attendeePreview).setVisibility(View.GONE);
            Toast.makeText(MainActivity.this, R.string.attendee_registered, Toast.LENGTH_SHORT).show();
        }
    }
}
