package com.cronyapps.odoo.core.orm.sync.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.cronyapps.odoo.api.wrapper.helper.OdooUser;
import com.cronyapps.odoo.core.orm.BaseDataModel;
import com.cronyapps.odoo.core.orm.sync.DataSyncAdapter;

public class AppSyncUtils {

    private Context context;
    private OdooUser user;
    private BaseDataModel model;

    private AppSyncUtils(BaseDataModel model) {
        this.model = model;
        context = model.getContext();
        user = model.getOdooUser();
    }

    private AppSyncUtils(Context context, OdooUser user) {
        this.context = context;
        this.user = user;
    }

    public static AppSyncUtils get(BaseDataModel model) {
        return new AppSyncUtils(model);
    }

    public static AppSyncUtils get(Context context, OdooUser user) {
        return new AppSyncUtils(context, user != null ? user : OdooUser.get(context));
    }

    public void requestSync(String authority) {
        requestSync(authority, null);
    }

    public void requestSync(String authority, Bundle bundle) {
        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        if (model != null)
            settingsBundle.putString(DataSyncAdapter.KEY_MODEL_NAME, model.getModelName());
        if (bundle != null) {
            settingsBundle.putAll(bundle);
        }
        Log.d("AppSyncUtils", "Requesting sync " + authority);
        ContentResolver.requestSync(user.account, authority, settingsBundle);
    }
}
