package com.cronyapps.odoo.core.orm.type;

import com.cronyapps.odoo.core.orm.utils.FieldType;


public class FieldBlob extends FieldType<FieldBlob, String> {

    public FieldBlob(String label) {
        super(label);
    }

    @Override
    public String columnType() {
        return "BLOB";
    }
}
