package com.cronyapps.odoo.core.utils;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import com.cronyapps.odoo.R;
import com.cronyapps.odoo.core.helper.CronyActivity;

import java.util.ArrayList;
import java.util.List;

public class OPermissionManager implements CronyActivity.PermissionResultListener {
    public static final int REQUEST_PERMISSION = 123;
    private CronyActivity mActivity;
    private PermissionStatusListener recentPermissionRequest;

    public OPermissionManager(CronyActivity activity) {
        mActivity = activity;
        mActivity.setPermissionResultListener(this);
    }

    public boolean hasPermissions(String... permissions) {
        for (String permission : permissions) {
            int permissionCheck = ContextCompat.checkSelfPermission(mActivity, permission);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }


    public void getPermissions(PermissionStatusListener callback, String... permissions) {
        recentPermissionRequest = callback;
        for (String permission : permissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permission)) {
                if (callback != null)
                    callback.requestRationale(permission);
                return;
            }
        }
        ActivityCompat.requestPermissions(mActivity, permissions, REQUEST_PERMISSION);
    }


    @Override
    public void onResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION && recentPermissionRequest != null) {
            if (grantResults.length > 0) {
                List<String> granted = new ArrayList<>();
                List<String> denied = new ArrayList<>();
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        granted.add(permissions[i]);
                    } else {
                        denied.add(permissions[i]);
                    }
                }
                recentPermissionRequest.granted(granted.toArray(new String[granted.size()]));
                recentPermissionRequest.denied(denied.toArray(new String[denied.size()]));
            }
        }
    }

    public void showRequestRationale(String permission) {
        String description = mActivity.getString(R.string.permission_rationale_format);
        String message = permissionReadable(permission);
        AlertDialog.Builder mRationalDialog = new AlertDialog.Builder(mActivity);
        mRationalDialog.setCancelable(false);
        mRationalDialog.setTitle(R.string.rational_dialog_title);
        mRationalDialog.setMessage(String.format(description, message));
        mRationalDialog.setPositiveButton(R.string.rational_dialog_button_grant,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        showAppSettingInfo();
                        dialog.dismiss();
                    }
                });
        mRationalDialog.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        mRationalDialog.show();
    }

    private void showAppSettingInfo() {
        // Ignoring api level check. Using 14+
        String appPackage = mActivity.getApplicationInfo().packageName;
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.parse("package:" + appPackage));
        mActivity.startActivity(intent);
    }

    private String permissionReadable(String permission) {
        String[] permissionParts = permission.split("\\.");
        try {
            PermissionInfo permissionInfo = mActivity.getPackageManager()
                    .getPermissionInfo(permission, PackageManager.GET_META_DATA);
            return permissionInfo.loadLabel(mActivity.getPackageManager()).toString();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return permissionParts[permissionParts.length - 1].replaceAll("_", " ").toLowerCase();
    }
}