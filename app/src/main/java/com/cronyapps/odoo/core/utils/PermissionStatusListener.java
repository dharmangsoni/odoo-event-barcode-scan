package com.cronyapps.odoo.core.utils;

public interface PermissionStatusListener {

    void requestRationale(String permission);

    void granted(String[] permissions);

    void denied(String[] permissions);
}