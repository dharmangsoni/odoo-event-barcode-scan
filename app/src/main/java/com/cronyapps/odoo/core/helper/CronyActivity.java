package com.cronyapps.odoo.core.helper;

import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.cronyapps.odoo.api.wrapper.helper.OdooUser;
import com.cronyapps.odoo.core.orm.sync.utils.AppSyncUtils;

public abstract class CronyActivity extends AppCompatActivity {


    public View getContentView() {
        return findViewById(android.R.id.content);
    }

    public AppSyncUtils syncUtils(OdooUser user) {
        return AppSyncUtils.get(this, user);
    }

    public OdooUser getUser() {
        return OdooUser.get(this);
    }

    private PermissionResultListener permissionResultListener;

    public void setPermissionResultListener(PermissionResultListener callback) {
        permissionResultListener = callback;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (permissionResultListener != null) {
            permissionResultListener.onResult(requestCode, permissions, grantResults);
        }
    }

    public interface PermissionResultListener {
        void onResult(int requestCode, String[] permissions, int[] grantResults);
    }

}
