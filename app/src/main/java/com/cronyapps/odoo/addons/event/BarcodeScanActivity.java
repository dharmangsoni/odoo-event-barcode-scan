package com.cronyapps.odoo.addons.event;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import com.cronyapps.odoo.R;
import com.cronyapps.odoo.core.helper.CronyActivity;
import com.cronyapps.odoo.core.utils.OPermissionManager;
import com.cronyapps.odoo.core.utils.PermissionStatusListener;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

public class BarcodeScanActivity extends CronyActivity
        implements SurfaceHolder.Callback, Detector.Processor<Barcode> {
    public static final String KEY_CONTENT = "key_content";
    private SurfaceView cameraPreview;
    private BarcodeDetector qrDetector;
    private CameraSource cameraSource;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_barcode_scan_activity);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.scan_code);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        setResult(RESULT_CANCELED);

        final OPermissionManager permissionManager = new OPermissionManager(this);
        if (!permissionManager.hasPermissions(Manifest.permission.CAMERA)) {
            permissionManager.getPermissions(new PermissionStatusListener() {
                @Override
                public void requestRationale(String permission) {
                    permissionManager.showRequestRationale(Manifest.permission.CAMERA);
                }

                @Override
                public void granted(String[] permissions) {
                    if (permissions.length > 0)
                        initDetector();
                }

                @Override
                public void denied(String[] permissions) {
                    if (permissions.length > 0) {
                        Toast.makeText(BarcodeScanActivity.this, R.string.toast_camera_permission_required_qr,
                                Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            }, Manifest.permission.CAMERA);
            return;
        }
        initDetector();
    }

    private void initDetector() {
        cameraPreview = (SurfaceView) findViewById(R.id.qrScanSurfaceView);
        cameraPreview.getHolder().addCallback(this);

        qrDetector = new BarcodeDetector.Builder(this).setBarcodeFormats(Barcode.ALL_FORMATS).build();
        if (!qrDetector.isOperational()) {
            // @see : https://developers.google.com/android/reference/com/google/android/gms/vision/Detector#public-methods
            Toast.makeText(BarcodeScanActivity.this, R.string.toast_dependencies_download_in_progress,
                    Toast.LENGTH_SHORT).show();
            finish();
        }
        qrDetector.setProcessor(this);
        cameraSource = new CameraSource.Builder(this, qrDetector).setAutoFocusEnabled(true).build();
    }

    public void onBarcodeScanned(final String content) {
        Log.i("QRScanner", content);
        Intent data = new Intent();
        data.putExtra(KEY_CONTENT, content);
        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("MissingPermission")
    private void startCameraPreview() {
        try {
            cameraSource.start(cameraPreview.getHolder());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        startCameraPreview();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        cameraSource.stop();
    }

    @Override
    public void release() {

    }

    @Override
    public void receiveDetections(Detector.Detections<Barcode> detections) {
        SparseArray<Barcode> items = detections.getDetectedItems();
        if (detections.detectorIsOperational() && items.size() != 0) {
            onBarcodeScanned(items.valueAt(0).displayValue);
        }
    }
}
