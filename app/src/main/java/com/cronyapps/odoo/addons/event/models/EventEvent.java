package com.cronyapps.odoo.addons.event.models;

import android.content.Context;

import com.cronyapps.odoo.api.wrapper.helper.OdooUser;
import com.cronyapps.odoo.core.orm.BaseDataModel;
import com.cronyapps.odoo.core.orm.annotation.DataModel;
import com.cronyapps.odoo.core.orm.type.FieldChar;

@DataModel("event.event")
public class EventEvent extends BaseDataModel<EventEvent> {

    public FieldChar name = new FieldChar("Name");

    public EventEvent(Context context, OdooUser user) {
        super(context, user);
    }
}
