package com.cronyapps.odoo.addons.event.models;

import android.content.Context;

import com.cronyapps.odoo.api.wrapper.helper.OdooUser;
import com.cronyapps.odoo.base.addons.res.models.ResPartner;
import com.cronyapps.odoo.core.orm.BaseDataModel;
import com.cronyapps.odoo.core.orm.annotation.DataModel;
import com.cronyapps.odoo.core.orm.annotation.DataModelSetup;
import com.cronyapps.odoo.core.orm.annotation.ModelSetup;
import com.cronyapps.odoo.core.orm.type.FieldChar;
import com.cronyapps.odoo.core.orm.type.FieldManyToOne;
import com.cronyapps.odoo.core.orm.type.FieldSelection;

@DataModel("event.registration")
@DataModelSetup(ModelSetup.DEFAULT)
public class EventRegistration extends BaseDataModel<EventRegistration> {

    public FieldChar name = new FieldChar("Name");
    FieldChar phone = new FieldChar("Phone");
    FieldChar email = new FieldChar("Email");
    public FieldManyToOne partner_id = new FieldManyToOne("Partner", ResPartner.class);
    public FieldManyToOne event_id = new FieldManyToOne("Event", EventEvent.class);
    public FieldChar barcode = new FieldChar("Barcode");
    public FieldSelection state = new FieldSelection("State")
            .addSelection("draft", "Unconfirmed")
            .addSelection("cancel", "Cancelled")
            .addSelection("open", "Confirmed")
            .addSelection("done", "Attended");

    public EventRegistration(Context context, OdooUser user) {
        super(context, user);
    }
}
